export const APP_URI: string = 'http://localhost:8080/';
export const AUTH_CODE: string = 'Basic ' +
    'YjE1MTBjM2QtMDFjYi00YWZhLTg1MjEtNDIzMDUzM2Y4MTJhOjlkNTcyMDU2LTk0ODAtNGYxYi05NWVmLTUyYzAxZmQ4OTgxOQ==';
export const AUTH_URI: string = 'https://api.secure.mercedes-benz.com/oidc10/auth/oauth/v2/token';
export const CAR_STATE_URI: string = 'https://cors-anywhere.herokuapp.com/https://api.mercedes-benz.com/' +
    'experimental/connectedvehicle/v1/vehicles/E20E8F841E7D6E0528/doors';
export const REDIR_URI: string = 'https://api.secure.mercedes-benz.com/oidc10/auth/oauth/v2/authorize' +
    '?response_type=code&client_id=b1510c3d-01cb-4afa-8521-4230533f812a' +
    '&redirect_uri=' + APP_URI + '&scope=mb:vehicle:status:general mb:user:pool:reader';
