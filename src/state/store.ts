import * as Cookies from 'js-cookie';
import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import { IState } from './istate';
import { mutations } from './mutations';

Vue.use(Vuex);

const vuexCookie = new VuexPersistence<IState>({
    filter: mutation => mutation.type === 'updateAccessToken',
    reducer: state => ({ accessToken: state.accessToken }),
    restoreState: key => Cookies.getJSON(key),
    saveState: (key, state) => Cookies.set(key, state, { expires: 1 / 24 }),
});

export default new Vuex.Store<IState>({
    mutations,
    plugins: [ vuexCookie.plugin ],
    state: {
        accessToken: '',
        doorStatus: 'WAITING',
    },
});
