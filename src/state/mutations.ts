import { IState } from './istate';

export const mutations = {
    updateAccessToken: (state: IState, payload: string): void => { state.accessToken = payload; },
    updateDoorStatus: (state: IState, payload: string): void => { state.doorStatus = payload; },
};
