const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: './index.ts'
    },
    module: {
	    rules: [
            {test: /\.ts$/, exclude: /node_modules/, use: ['ts-loader']},
            {test: /\.html/, exclude: /node_modules/, use: ['html-loader']},
	        {test: /\.scss$/, exclude: /node_modules/, use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']},
	    ]
	},
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.ts', '.js', '.css', '.scss', '.html', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    plugins: [new HtmlWebpackPlugin({template: './index.html'})],
};