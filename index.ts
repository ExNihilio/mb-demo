import Vue from 'vue';
import VueResource from 'vue-resource';
import { APP_URI, AUTH_CODE, AUTH_URI, CAR_STATE_URI, REDIR_URI } from './src/config/config';
import store from './src/state/store';
import './style.scss';

Vue.use(VueResource);

const app = new Vue({
    computed: {
        getIndicatorClass: (): string => {
            return (store.state.doorStatus === 'LOCKED' ? 'lock' :
                (store.state.doorStatus === 'UNLOCKED' ? 'unlock' : 'waiting'));
        },
        getState: (): string => store.state.doorStatus,
    },
    // tslint:disable-next-line: object-literal-shorthand
    created: function(){ this.parseQueryParams(); },
    data: () => {
        return {
            appState: 'Please Wait...',
            getRequest: null,
            getTimeout: undefined,
            loaded: false,
            queryParams: {},
        };
    },
    el: '#app',
    methods: {
        // tslint:disable-next-line: object-literal-shorthand
        changeDoorStatus: function(): void{
            if(store.state.doorStatus === 'WAITING') return;
            if(this.getTimeout) clearTimeout(this.getTimeout);
            const cmd = store.state.doorStatus === 'LOCKED' ? 'UNLOCK' : 'LOCK';
            store.commit('updateDoorStatus', 'WAITING');
            this.$http
                .post(
                    CAR_STATE_URI,
                    {
                        command: cmd,
                        before(request) {
                            if(this.previousRequest) this.previousRequest.abort();
                            this.previousRequest = request;
                        },
                    },
                    {
                        headers: { Authorization: 'Bearer ' + store.state.accessToken },
                    },
                )
                .then(
                    () => { this.getDoorStatus(); },
                    () => { this.getDoorStatus(); },
                );
        },
        // tslint:disable-next-line: object-literal-shorthand
        getDoorStatus: function(): void{
            if(this.getTimeout) clearTimeout(this.getTimeout);
            this.$http
                .get(
                    CAR_STATE_URI,
                    {
                        headers: { Authorization: 'Bearer ' + store.state.accessToken },
                        before(request) {
                            if(this.previousRequest) this.previousRequest.abort();
                            this.previousRequest = request;
                        },
                    },
                )
                .then(
                    success => {
                        store.commit('updateDoorStatus', success.data.doorlockstatusvehicle.value);
                        this.loaded = true;
                        (this.getTimeout as any) = setTimeout(this.getDoorStatus, 10000);
                    },
                    () => {
                        this.loaded = true;
                        (this.getTimeout as any) = setTimeout(this.getDoorStatus, 10000);
                    },
                );
        },
        // tslint:disable-next-line: object-literal-shorthand
        outhAuthenticate: function(): void{
            this.$http
                .post(
                    AUTH_URI,
                    // tslint:disable-next-line: no-string-literal
                    'grant_type=authorization_code&code=' + this.queryParams['code'] + '&redirect_uri=' + APP_URI,
                    { headers: { 'content-type': 'application/x-www-form-urlencoded', Authorization: AUTH_CODE } },
                )
                .then(
                    success => {
                        store.commit('updateAccessToken', success.data.access_token);
                        window.location.href = APP_URI;
                    },
                    () => { this.appState = 'Couldn\'t authenticate. Please refresh the page to try again.'; },
                );
        },
        outhRedirection: (): void => { window.location.href = REDIR_URI; },
        // tslint:disable-next-line: object-literal-shorthand
        parseQueryParams: function(): void{
            const regex: RegExp = new RegExp('([^=&]+?)=([^=&]+)', 'g');
            let matches: RegExpExecArray | null = regex.exec(window.location.search.substr(1));
            while(matches !== null){
                this.queryParams[matches[1]] = matches[2];
                matches = regex.exec(window.location.search.substr(1));
            }
            if(!store.state.accessToken)
                // tslint:disable-next-line: no-string-literal
                if(!this.queryParams['code']) this.outhRedirection();
                else this.outhAuthenticate();
            else this.getDoorStatus();
        },
    },
    store,
    template: require('./src/app/app.component.html'),
});
